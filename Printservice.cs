﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;

namespace Print_Label
{
    public class Printservice
    {
        private readonly Timer _timer;

        static DataTable Print_Label = new DataTable();
        Connections sh = new Connections();

        SQLConnecton sg = new SQLConnecton();

        public string Tag_Name { get; set; }
        public string zone_Id { get; set; }
        public Printservice()
        {

            _timer = new Timer(30000) { AutoReset = true };
            _timer.Elapsed += timerElapsed;
           

        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            string[] lines = new string[] { "UWB Alert Service Started : " + DateTime.Now.ToString() };
            File.AppendAllLines(@"C:\Demo\UwbAlertservice.txt", lines);

            GetMainZones();
        }

        private void GetMainZones()
        {
            Console.WriteLine("Fetching data  from mysql");
           Console.WriteLine(DateTime.Now);

            DataTable Mainzones = sh.Fetch_MainZones();
            foreach (DataRow row in Mainzones.Rows)
            {
                //Console.WriteLine("for loop");
                var Tag_Name = row[1].ToString();
                var Zone_Id = row[2].ToString();
                Console.WriteLine("Tag Name :  "+Tag_Name + "Zone id :" +Zone_Id);
                try
                {

                    var Mhe_Id = Tag_Name;
            //        Console.WriteLine("Data Fetched from mysql zone :171 Tag Name: " + Mhe_Id);

                    GetProduction_DE(Tag_Name, Zone_Id);
                    check_3minCond(Tag_Name, Zone_Id);
                }
                catch (Exception ex)
                {
           //         Console.WriteLine(ex);
                    string[] lines = new string[] { ex + " on: " + DateTime.Now.ToString() };
                    File.AppendAllLines(@"C:\Demo\UwbAlertservice.txt", lines);
                }
            }
        }


        public void GetProduction_DE(string Tag_Name, string Zone_Id)
        {
            try
            {
                var PK_Dext_Id = (dynamic)null;
                var MHE_Name = (dynamic)null;

           //     Console.WriteLine("fetching mainzones ");
                sh.UpdateMainZones(Tag_Name);
                DataTable Production = sg.Fetch_Production(Tag_Name);
                if (Production.Rows.Count > 0)
                {
                    DataRow row = Production.Rows[0];
                    var Mhe_Id = row[0].ToString();
                    var Mhe_Name = row[1].ToString();
               //     Console.WriteLine("Invoked in  Zone id : " + Zone_Id + "  Tag_Name : " + Tag_Name);
                    string[] lines = new string[] { "Invoked in DE  Tag Name :" + Tag_Name + " Date: " + DateTime.Now.ToString() };
                    File.AppendAllLines(@"C:\Demo\UwbAlertservice.txt", lines);
                    sg.UpdateProduction(Mhe_Name);
                    

                }
                else
                {
                    Console.WriteLine("Invoked in Alerts, Tag Name :" + Tag_Name);
                    string[] lines = new string[] { "Invoked in Alerts, Tag Name :" + Tag_Name + " Date: " + DateTime.Now.ToString() };
                    File.AppendAllLines(@"C:\Demo\UwbAlertservice.txt", lines);
                    check_3minCond(Tag_Name, Zone_Id);
                   
                }
            }
            catch (Exception ex)
            {

               // Console.WriteLine(ex);
                string[] lines = new string[] { ex + " on: " + DateTime.Now.ToString() };
                File.AppendAllLines(@"C:\Demo\UwbAlertservice.txt", lines);
            }

        }
        public void check_3minCond(string Tag_Name, string Zone_Id)
        {
            DataTable Alerts = sg.Fetch_Alerts();
            if (Alerts.Rows.Count > 0)
            {
                //      Console.WriteLine("Inside alerts Tag_Name: " + Tag_Name);
                DataRow row = Alerts.Rows[0];
                var tad_name = row[1].ToString();
                var zone_Id = row[2].ToString();
               // Console.WriteLine("Tag Name : " + tad_name + "zone id  : " + zone_Id);
                var updateddatetime = row[5];
                //Console.WriteLine("updated time: " + updateddatetime.ToString());

                if (string.IsNullOrEmpty(updateddatetime.ToString()))
                {
                  
                    // Console.WriteLine("entry datetime " + Convert.ToDateTime(row[4].ToString()));
                    //  Console.WriteLine("current datetime" + DateTime.Now.ToString("MM/dd/yyyy H:mm"));
                    var currentTime = DateTime.Now;
                    //Console.WriteLine("Current time :" + currentTime);
                    var entryTime = Convert.ToDateTime(row[4].ToString());

                  
                    //Console.WriteLine("Entry time :" + entryTime);

                    TimeSpan diffdateTime = currentTime - entryTime;
                    //Console.WriteLine("date time diff " + diffdateTime.TotalMinutes);
                    if (diffdateTime.TotalSeconds >= 180)
                    {   
                      //  Console.WriteLine("If datetime is >= 3 min" + diffdateTime.ToString());
                        DataTable TRUCKNO = sg.Fetch_tagname(Tag_Name);
                        DataRow r = TRUCKNO.Rows[0];
                        var mhe_Name = r[6].ToString();
                       // Console.WriteLine("leaf truck at out side zone with 3 min or more " + mhe_Name);
                        sg.updatetreadentry(mhe_Name);
                        //Console.WriteLine("updated tread entry  " + mhe_Name);
                        sg.updatealerts(Tag_Name);
                    //    Console.WriteLine("updated alerts   " + Tag_Name);
                    }
                    else
                    {
                       Master_default(Tag_Name, Zone_Id);
                    }
                }
                else
                {
                    Master_default(Tag_Name, Zone_Id);
                }
            }
            else
            {
                Master_default(Tag_Name, Zone_Id);
            }

        }
        public void Master_default(string Tag_Name, string Zone_Id)
        {
          //  Console.WriteLine("checking treadentry");
            DataTable fetchtreadentry = sg.fetchtreadentry(Tag_Name);
            if (fetchtreadentry.Rows.Count > 0)
            {
                
                DataRow row1 = fetchtreadentry.Rows[0];
               
                var Pkdextid = Convert.ToInt32(row1[0].ToString());
               // Console.WriteLine( "pkdid :"+Pkdextid +"+ Tag Name :"+ Tag_Name +"+ Zone id :"+ Zone_Id);
                sg.InsertIntoAlerts(Tag_Name, Zone_Id, Pkdextid);
               // Console.WriteLine("Inserted to alerts");
            }
            else
            {
                //if no data in production table 
               // Console.WriteLine("pkdext id not found");
            }
        }
        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}
