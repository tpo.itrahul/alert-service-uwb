﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print_Label
{
    class SQLConnecton
    {
        public SqlConnection con;
        public SqlConnection con1;
        public SqlConnection con2;
        public SqlDataAdapter ADP = new SqlDataAdapter();
        public SqlDataAdapter ADP1 = new SqlDataAdapter();
        public SqlDataAdapter ADP2 = new SqlDataAdapter();

        public void getConnection()
        {
            try
            {
                con = new SqlConnection();
                con.Close();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con.ConnectionString = connectionString;
                con.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }

        }



        public void getConnection1()
        {
            try
            {
                con1 = new SqlConnection();
                con1.Close();
                if (con1.State == ConnectionState.Open)
                {
                    con1.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con1.ConnectionString = connectionString;
                con1.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }

        }
        public void getConnection2()
        {
            try
            {
                con2 = new SqlConnection();
                con2.Close();
                if (con2.State == ConnectionState.Open)
                {
                    con2.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con2.ConnectionString = connectionString;
                con2.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con2.Close();
            }

        }


        public int Execute_Query1(string sql_query)
        {
            int result = 0;
            getConnection1();
            try
            {
                SqlCommand cmd1 = new SqlCommand(sql_query, con1);
                result = cmd1.ExecuteNonQuery();
                con1.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }
            return result;
        }

        public int Execute_Query2(string sql_query)
        {
            int result = 0;
                getConnection2();
            try
            {
                SqlCommand cmd2 = new SqlCommand(sql_query, con2);
                result = cmd2.ExecuteNonQuery();
                con2.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con2.Close();
            }
            return result;
        }

        public DataTable DataRetrieval(string strcon)
        {
            getConnection();
            DataTable dt_Schedule = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(strcon, con);
                ADP.SelectCommand = cmd;
                ADP.Fill(dt_Schedule);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }
            return dt_Schedule;
        }
        //public DataTable DataRetrieval1(string strcon)
        //{
        //    getConnection1();
        //    DataTable dt_Schedule = new DataTable();
        //    try
        //    {
        //        SqlCommand cmd1 = new SqlCommand(strcon, con1);
        //        ADP1.SelectCommand = cmd1;
        //        ADP1.Fill(dt_Schedule);
        //        con1.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Write(ex);
        //        con1.Close();
        //    }
        //    return dt_Schedule;
        //}

        public DataTable Fetch_Production(string tag_Name)
        {
            DataTable dt_Data = new DataTable();
            string ls_sql = "Select MP.UWB_Tag_Id,MP.MHE_Name From [FIFO].[dbo].[DEXT_TBREQUEST] RQ  left outer join [TraceMeDB].[dbo].[UWB_Tag_Mapping] MP on RQ.truckno = MP.MHE_Name  where  RQ.RequestStatus = 0 and  MP.UWB_Tag_Id='" + tag_Name + "'  order by RQ.req_time asc";
            dt_Data = DataRetrieval(ls_sql);
            return dt_Data;
        }
        public DataTable fetchtreadentry(string tag_Name)
        {
            DataTable dt_Data = new DataTable();
            //string ls_sql = "Select MP.UWB_Tag_Id,MP.MHE_Name From [FIFO].[dbo].[DEXT_TBREQUEST] RQ  left outer join [TraceMeDB].[dbo].[UWB_Tag_Mapping] MP on RQ.truckno = MP.MHE_Name  where  RQ.RequestStatus = 0 and  MP.UWB_Tag_Id='" + tag_Name + "'  order by RQ.req_time asc";
            string ls_sql = "select pr.PKDEXTID from [FIFO].[dbo].[DEXT_TREADENTRY] pr right  join  [TraceMeDB].[dbo].[UWB_Tag_Mapping] mp on pr.LEAF_TRUCKNO = mp.MHE_Name  where pr.status ='false'and pr.hold ='false' and pr.scrap = 'false'  and mp.UWB_Tag_Id = '" + tag_Name + "'";
            dt_Data = DataRetrieval(ls_sql);
            return dt_Data;
        }

        public DataTable Fetch_Alerts()
        {
            DataTable dt_Data = new DataTable();
            string ls_sql = "Select * From UWB_Alerts where status = 0 ";
            dt_Data = DataRetrieval(ls_sql);
            return dt_Data;
        }
        public DataTable Fetch_tagname(string tag_Name)
        {
            DataTable dt_Data = new DataTable();
            string ls_sql = "Select * From UWB_Tag_Mapping where Tag_Name='" + tag_Name + "' and status=1 ";
            dt_Data = DataRetrieval(ls_sql);
            return dt_Data;
        }

        public int updatealerts(string tag_Name)
        {

            string ls_sql = "update UWB_Alerts set Upadated_Date_Time ='" + DateTime.Now.ToString("yyyy-MM-dd H:mm") + "' where Tag_Name = '" + tag_Name + "' and status = 0 ";
            return Execute_Query1(ls_sql);
        }
        public int updatetreadentry(string tag_Name)
        {

            string ls_sql = "update  [FIFO].[dbo].[DEXT_TREADENTRY] set STATUS ='TRUE' where truckno = '" + tag_Name + "' and STATUS ='FALSE' ";
            return Execute_Query1(ls_sql);
        }


        public int UpdateProduction(string Mhe_Name)
        {
            string ls_sql = "update [FIFO].[dbo].[DEXT_TBREQUEST] set RequestStatus = 1  where truckno ='" + Mhe_Name + "'";
            return Execute_Query1(ls_sql);
        }
        public int InsertIntoAlerts(string tag_Name, string Zone_Id, int pkdextid)
        {
            string ls_sql = "INSERT INTO  UWB_Alerts (Tag_Name,Zone_Id,Date_Time,Status,Extruder_Tag_No) Values('" + tag_Name + "','" + Zone_Id + "','" + DateTime.Now.ToString("yyyy-MM-dd H:mm") + "',0,'" + pkdextid + "')";
            return Execute_Query2(ls_sql);
        }



    }

}

