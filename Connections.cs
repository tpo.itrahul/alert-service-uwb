﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print_Label
{
    class Connections
    {
        public MySqlConnection con;
        public MySqlConnection con1;
        public MySqlDataAdapter ADP = new MySqlDataAdapter();
        public MySqlDataAdapter ADP1 = new MySqlDataAdapter();

        public void GetConnection()
        {
            try
            {
                con = new MySqlConnection();
                con.Close();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMySQL"].ConnectionString;
                con.ConnectionString = connectionString;
                con.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }
        }

        public void GetConnection1()
        {
            try
            {
                con1 = new MySqlConnection();
                con1.Close();
                if (con1.State == ConnectionState.Open)
                {
                    con1.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMySQL"].ConnectionString;
                con1.ConnectionString = connectionString;
                con1.Open();

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }
        }



        public int Execute_Query(string sql_query)
        {
            int result = 0;
            GetConnection();
            try
            {
                MySqlCommand cmd = new MySqlCommand(sql_query, con);
                result = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con.Close();
            }
            return result;
        }
        public DataTable DataRetrieval(string strcon)
        {
            GetConnection1();
            DataTable dt_Data = new DataTable();
            try
            {
                MySqlCommand cmd1 = new MySqlCommand(strcon, con1);
                ADP1.SelectCommand = cmd1;
                ADP1.Fill(dt_Data);
                con1.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                con1.Close();
            }
            return dt_Data;
        }

        public DataTable Fetch_MainZones()
        {
            DataTable dt_Data = new DataTable();
            string print_sc = "Select * from UWB_Main_Zoneupdates where Fetching_Status= 0 and action='Out' and Zone_id in ('171','146') and Tag_Name not in (Select Tag_Name from UWB_Reserved_Position where Zone_Id is not null and Zone_Id in ('173','145','157','158','167'))";
            dt_Data = DataRetrieval(print_sc);
            return dt_Data;
        }


        public int UpdateMainZones(string tag_Name)
        {
            string ls_sql = "update UWB_Main_Zoneupdates set Fetching_Status = 1 where Tag_Name ='" + tag_Name + "'";
            return Execute_Query(ls_sql);
        }


    }

}
